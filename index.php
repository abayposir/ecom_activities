<?php

require 'core/db.php';
require 'core/functions.php';

$sql = "SELECT products.* , product_categories.name as category
        FROM products
        JOIN product_categories
        ON product_categories.id = products.category_id;";

$query = $conn->query($sql);
$products = mysqli_fetch_all($query,MYSQLI_ASSOC);
$conn->close();


require 'views/index.view.php';


?>
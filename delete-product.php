<?php
	require 'core/db.php';
	require 'core/functions.php';


  $sql = $conn->prepare("SELECT * FROM products WHERE id=?");
  $sql->bind_param("i",$_GET["id"]);
  $sql->execute();

  $result = $sql->get_result();
  if ($result->num_rows > 0) {
    $product = $result->fetch_assoc();
    
  }else{

    header('Location: products.php');
  }


	$sql = $conn->prepare("DELETE  FROM products WHERE id=?");
	$sql->bind_param("i", $_GET["id"]);
	$sql->execute();
	$sql->close();
	$conn->close();

	header('location:products.php');

?>
<?php include 'includes/header.php';?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="mt-5">Welcome to My Shop</h1>
        
        </div>
      </div><hr>

      <div class="row">

        <?php foreach ($products as $product): ?>
          <div class="col-lg-4">
            <div  class="card" style="width: 20rem;min-height:245px;">
          
              <div class="card-body">
                <h4 class="card-title"><?= $product['name']; ?></h4>
                <span class="badge badge-warning"><?= $product['category']; ?></span>
                <span class="badge badge-warning"><?= $product['short_desc']; ?></span>
                <span class="badge badge-warning"><?= $product['long_desc']; ?></span>
              </div>
            </div>
          </div>
        <?php endforeach; ?>

      </div>


    </div>

<?php include 'includes/footer.php';?>
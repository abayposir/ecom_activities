<?php include 'includes/header.php';?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="mt-5">Create Category</h3>
          <a href="categories.php" class="btn btn-secondary btn-sm">Back to Category List</a><hr>

          <div class="col-lg-8">

            <!-- SHOW AN ALERT MESSAGE IF A USER SUCCESSFULLY ADDED A CATEGORY -->
            <?php if (!empty($success_message)): ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?= $success_message ?>
              </div>
            <?php endif; ?>


            <!-- SHOW AN ALERT MESSAGE IF A USER FAILED TO ADD A CATEGORY -->
            <?php if (!empty($error_message)): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?= $error_message ?>
              </div>
            <?php endif; ?>

            <form method="post" value="">
              <div class="form-group">
                <label for="name">Name</label>
                <input name="name" type="text" class="form-control" placeholder="Enter Category Name" required>
              </div>
              <div class="form-group">
                <label for="description">Name</label>
                <input name="description" type="text"  class="form-control"  placeholder="Enter Category Description" required>
              </div>

              <button type="submit" name="add-category" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>



<?php include 'includes/footer.php';?>
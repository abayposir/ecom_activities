<?php include 'includes/header.php';?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="mt-5">Categories</h1>
          <a href="add-category.php" class="btn btn-secondary btn-sm">Create Categories</a><hr>
          <table  id="categories" class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($categories as $category): ?>
              <tr>
                <th scope="row"><?= $category['id']; ?></th>
                <td><?= $category['name']; ?></td>
                <td><?= $category['description']; ?></td>
                <td>
                  <a class="btn btn-primary" href="edit-category.php?id=<?=$category['id'] ?>" role="button">Edit</a>
                  <a class="btn btn-danger" href="delete-category.php?id=<?=$category['id'] ?>" role="button">Delete</a>
                </td>
              </tr>
            <?php endforeach; ?>

          </tbody>
        </table>
        </div>
      </div>
    </div>


    <!--  SCRIPT TO TRIGGER THE #categories DATATABLES -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('#categories').DataTable();
    } );
  </script>


<?php include 'includes/footer.php';?>
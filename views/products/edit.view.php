<?php include 'includes/header.php';?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="mt-5">Edit Product - <?= $product['name']; ?></h3>
          <a href="products.php" class="btn btn-secondary btn-sm">Back to Product List</a><hr>

          <div class="col-lg-8">
            <!-- SHOW AN ALERT MESSAGE IF A USER SUCCESFULLY UPDATE A PRODUCT -->
            <?php if (!empty($success_message)): ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?= $success_message ?>
              </div>
            <?php endif; ?>
            <!-- SHOW AN ALERT MESSAGE IF A USER FAILED TO UPDATE A PRODUCT -->
            <?php if (!empty($error_message)): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?= $error_message ?>
              </div>
            <?php endif; ?>

            <form enctype="multipart/form-data" method="post" value="">
          
              <div class="form-group">
                <label for="name">Name</label>
                <input name="name" type="text" class="form-control" placeholder="Enter Product Name"  value="<?= $product['name']?>" required>
              </div>

              <div class="form-group">
                <label for="category">Category</label>
                <select name="category_id" class="form-control">
                  <?php foreach ($categories as $category): ?>
                    <?php if ($category['id'] == $product['category_id']): ?>
                        <option value="<?= $category['id']; ?>" selected><?= $category['name']; ?></option>
                    <?php else: ?>
                        <option value="<?= $category['id']; ?>"><?= $category['name']; ?></option>
                    <?php endif; ?>

                  <?php endforeach; ?>
                </select>
              </div>

              <div class="form-group">
                <label for="short_desc">Short Description</label>
                <input name="short_desc"  type="text"  class="form-control"  placeholder="Enter short description"  required>
              </div>

              <div class="form-group">
                <label for="long_desc">Short Description</label>
                <input name="long_desc" type="text"   class="form-control"  placeholder="Enter long description" required>
              </div>

              <button type="submit" name="update-product" class="btn btn-primary">Submit</button>
            </form>

            <br>

          </div>
        </div>
      </div>
    </div>



<?php include 'includes/footer.php';?>
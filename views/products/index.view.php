<?php include 'includes/header.php';?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="mt-5">Products</h1>
          <a href="add-product.php" class="btn btn-secondary btn-sm">Create Products</a><hr>
          <table  id="products" class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Image</th>
              <th>Name</th>
              <th>Category</th>
              <th>Short Description</th>
              <th>Long Description</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $product): ?>
              <tr>
                <th scope="row"><?= $product['id']; ?></th>
             
                <td><?= $product['name']; ?></td>
                <td><?= $product['category']; ?></td>
                <td><?= $product['short_desc']; ?></td>
                <td><?= $product['long_desc']; ?></td>
                <td>
                  <a class="btn btn-primary" href="edit-product.php?id=<?=$product['id'] ?>" role="button">Edit</a>
                  <a class="btn btn-danger" href="delete-product.php?id=<?=$product['id'] ?>" role="button">Delete</a>
                </td>
              </tr>
            <?php endforeach; ?>

          </tbody>
        </table>
        </div>
      </div>
    </div>

<!--  SCRIPT TO TRIGGER THE #products DATATABLES -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('#products').DataTable();
    } );
  </script>


<?php include 'includes/footer.php';?>
<?php
require 'core/db.php';
require 'core/functions.php';

if (isset($_POST['update-category'])) {

  $sql = $conn->prepare("UPDATE product_categories SET name=? , description=?   WHERE id=?");
  $name = $_POST['name'];
  $description = $_POST['description'];
  $sql->bind_param("sss",$name, $description,$_GET["id"]);
  if($sql->execute()) {
    $success_message = "Edited Successfully";
  } else {
    $error_message = "Problem in Editing Record";
  }

}


$sql = $conn->prepare("SELECT * FROM product_categories WHERE id=?");
$sql->bind_param("i",$_GET["id"]);
$sql->execute();
$result = $sql->get_result();


if ($result->num_rows > 0) {
  $category = $result->fetch_assoc();
}else{
  
  header('Location: categories.php');
}
$conn->close();


include 'views/categories/edit.view.php';




?>
<?php
require 'core/db.php';
require 'core/functions.php';


if (isset($_POST['update-product'])) {

  $sql = $conn->prepare("UPDATE products SET name=? , category_id=? , short_desc=?, ,long_desc=?   WHERE id=?");
  $name = $_POST['name'];
  $category_id = $_POST['category_id'];
  $old_image = $_POST['old_image'];


  $sql->bind_param("sisdi",$name, $category_id,$image,$price,$_GET["id"]);
  if($sql->execute()) {

    $success_message = "Edited Successfully";
  } else {
    $error_message = "Problem in Editing Record";
  }

}

// SELECT ALL PRODUCTS
$sql = $conn->prepare("SELECT * FROM products WHERE id=?");
$sql->bind_param("i",$_GET["id"]);
$sql->execute();
$result = $sql->get_result();
if ($result->num_rows > 0) {
  $product = $result->fetch_assoc();
  // dumper($product);
}else{
  header('Location: products.php');
}


// FETCH ALL PRODUCT CATEGORIES
$sql = "SELECT * FROM product_categories";
$query = $conn->query($sql);
$categories = mysqli_fetch_all($query,MYSQLI_ASSOC);
$conn->close();



include 'views/products/edit.view.php';




?>
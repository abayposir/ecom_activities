<?php
require 'core/db.php';
require 'core/functions.php';

if (isset($_POST['add-category'])) {

  $sql = $conn->prepare("INSERT INTO product_categories (name,description) VALUES (?, ?)");
  $name = $_POST['name'];
  $description= $_POST['description'];
  $sql->bind_param("ss", $name, $description);
  if($sql->execute()) {
    $success_message = "Added Successfully";
  } else {
    $error_message = "Problem in Adding New Record";
  }

  $sql->close();
  $conn->close();

}


include 'views/categories/create.view.php';


?>
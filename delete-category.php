<?php
	require 'core/db.php';
	require 'core/functions.php';
  // DELETE A CATEGORY USING  $_GET["id"] as the one to be deleted

	$sql = $conn->prepare("DELETE  FROM product_categories WHERE id=?");
	$sql->bind_param("i", $_GET["id"]);
	$sql->execute();
	$sql->close();
	$conn->close();

	// redirect to /categories.php
	header('location:categories.php');

?>
<?php
include '.class/product_class.php';
require 'core/db.php';
require 'core/functions.php';

if (isset($_POST['add-product'])) {
  
  $product =  new Product;

  
  $sql = $conn->prepare("INSERT INTO products (name,category_id,short_desc,long_desc) VALUES (?,?,?,?)");
  $name = $_POST['name'];
  $short_desc= $_POST['short_desc'];
  $long_desc= $_POST['long_desc'];
  $category_id= $_POST['category_id'];


  $sql->bind_param("sids", $name,$category_id, $short_desc, $long_desc);
  if($sql->execute()) {
    $success_message = "Added Successfully";
  } else {
    $error_message = "Problem in Adding New Record";
  }
  $sql->close();
}

$sql = "SELECT * FROM product_categories";
$query = $conn->query($sql);
$categories = mysqli_fetch_all($query,MYSQLI_ASSOC);
$conn->close();

include 'views/products/create.view.php';


?>